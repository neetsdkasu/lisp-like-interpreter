package myapp.util;

public class NullArgumentException extends IllegalArgumentException
{
	public NullArgumentException( String arg )
	{
		super( arg );
	}
	
}