package myapp.lisp;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import myapp.lisp.Engine;
import myapp.lisp.Lisp;
import myapp.lisp.Parser;
import myapp.util.NullArgumentException;

class CUI
{
	private final String PROMPT1 = "LISP>";
	private final String PROMPT2 = "    >";
	
	private Engine engine;
	private Lisp lisp;
	private Parser parser;
	private String prompt;
	
	CUI( Lisp lisp )
	{
		if ( lisp == null )
		{
			throw new NullArgumentException( "lisp" );
		}
		this.lisp = lisp;
		engine = lisp.getEngine();
		parser = lisp.getParser();
	}
	
	private void printStartMessage()
	{
		System.out.println( "-----------------------------------" );
		System.out.println( "        LISP   INTERPRETER         " );
		System.out.println( "-----------------------------------" );
		System.out.println( " Version " + lisp.getVersion()       );
		System.out.println(                                       );
		System.out.println( " 'ctrl-z' to exit this interpreter " );
		System.out.println(                                       );
		System.out.println( "-----------------------------------" );
		System.out.println(                                       );
	}
	
	private void printHelpText()
	{
		System.out.println( "[Help]"                                );
		System.out.println( " :bye      same as ':exit'"            );
		System.out.println( " :cancel   cancel inputting lisp code" );
		System.out.println( " :exit     exit interpreter"           );
		System.out.println( " :last     show last code"             );
		System.out.println( " :help     show this help text"        );
		System.out.println( " :quit     same as ':exit'"            );
		System.out.println(                                         );
	}
	
	private boolean doCommand( String cmd )
	{
		if ( ":bye".equals( cmd )
			|| ":exit".equals( cmd )
			|| ":quit".equals( cmd ) )
		{
			return ( true ) ;
		}
		switch ( cmd )
		{
			case ":help":
				{
					printHelpText();
				}
				break;
				
			case ":last":
				{
					System.out.println( "[Last Code]" );
					System.out.println( parser.getLastCode() );
				}
				break;
			
			case ":cancel":
				{
					System.out.println( "[Cancel]" );
					parser.reset();
					prompt = PROMPT1;
				}
				break;
			
			default:
				{
					System.out.println( "Error!! Unknown Command! -> " + cmd );
				}
				break;
		}
		return ( false ) ;
	}
	
	private String getIndent()
	{
		final String spacer = "                    ";
		int nest = parser.getNest();
		if ( nest == 0 )
		{
			return ( "" ) ;
		}
		else if ( nest * 2 < spacer.length() )
		{
			return ( spacer.substring( spacer.length() - nest * 2 ) ) ;
		}
		else
		{
			return ( spacer ) ;
		}
	}
	
	void start() throws java.lang.Exception
	{
		printStartMessage();
		
		BufferedReader in = new BufferedReader( new InputStreamReader( System.in ) );
		
		prompt = PROMPT1;
		String line;
		
		for (;;)
		{
			System.out.print( prompt + getIndent() );
			
			line = in.readLine();
			
			// End of Input
			if ( line == null )
			{
				break;
			}
			
			// Empty Line
			line = line.trim();
			if ( line.length() == 0 )
			{
				continue;
			}
			
			// System Command
			if ( line.charAt( 0 ) == ':' )
			{
				if ( doCommand( line.toLowerCase() ) )
				{
					break;
				}
				else
				{
					continue;
				}
			}
			
			// Lisp Code
			switch ( parser.input( line ) )
			{
				case COMPLETE:
					{
						if ( engine.excute() )
						{
							String result = engine.getResult();
							if ( result != null )
							{
								System.out.println( result );
							}
						}
						else
						{
							System.out.println( "Error!!" );
							System.out.println( engine.getErrorMessage() );
						}
						prompt = PROMPT1;
						parser.reset();
					}
					break;
					
				case MOREINPUT:
					{
						prompt = PROMPT2;
					}
					break;
					
				default:
					{
						System.out.println( "Token Error!!" );
						prompt = PROMPT1;
						parser.reset();
					}
					break;
			}
		}
	}
}
