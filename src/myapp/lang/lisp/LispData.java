package myapp.lisp;

abstract class LispData
{
	abstract boolean isAtom();
	abstract boolean isNil();
	abstract LispData car();
	abstract LispData cdr();
}
