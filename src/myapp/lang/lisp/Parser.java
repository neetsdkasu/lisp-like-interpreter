package myapp.lisp;

import java.lang.StringBuilder;

class Parser
{
	enum InputResult
	{
		COMPLETE ,
		MOREINPUT , 
		TOKENERROR
	}
	
	private int nest = 0;
	private StringBuilder currentcode;
	private String lastcode = "";

	Parser()
	{
		currentcode = new StringBuilder();
	}
	
	void reset()
	{
		nest = 0;
		lastcode = currentcode.toString();
		currentcode.delete( 0 , currentcode.length() );
	}
	
	int getNest()
	{
		return ( nest ) ;
	}
	
	String getLastCode()
	{
		return ( lastcode ) ;
	}
	
	
	
	InputResult input( String src )
	{
		int sp = 0;
		
		if ( nest != 0 )
		{
			sp = -1;
		}
		
		int len = src.length();
		for ( int i = 0 ; i < len ; i++ )
		{
			sp++;
			char ch = src.charAt( i );
			currentcode.append( ch );
			switch ( ch )
			{
				case ' ':
				case '\t':
				case '\n':
				case '\r':
					{
						if ( sp == 0 )
						{
							currentcode.delete( currentcode.length() - 1 , currentcode.length() );
						}
						sp = -1;
					}
					break;
				
				case '(': // Open Bracket
					{
						nest++;
						sp = -1;
					}
					break;
					
				case ')': // Close Bracket
					{
						if ( sp == 0 )
						{
							currentcode.delete( currentcode.length() - 2 , currentcode.length() - 1 );
						}
						nest--;
					}
					break;
					
				case ';': // Comment
					{
						currentcode.delete( currentcode.length() - 1 , currentcode.length() );
						i = len;
						sp--;
					}
					break;
					
				default: // Atom
					{
					}
					break;
			}
		}
		
		if ( nest == 0 )
		{
			return ( InputResult.COMPLETE ) ;
		}
		else if ( nest > 0 )
		{
			if ( sp >= 0 )
			{
				currentcode.append( ' ' );
			}
			return ( InputResult.MOREINPUT ) ;
		}
		else
		{
			return ( InputResult.TOKENERROR ) ;
		}
	}	
	
}