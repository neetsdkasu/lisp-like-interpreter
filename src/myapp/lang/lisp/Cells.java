package myapp.lisp;

import myapp.lisp.LispData;
import myapp.util.NullArgumentException;

class Cells extends LispData
{
	private final LispData left;
	private final LispData right;
	
	private static Cells nil = null;
	
	static Cells getNil()
	{
		if ( Cells.nil == null )
		{
			Cells.nil = new Cells();
		}
		return ( Cells.nil ) ;
	}
	
	private Cells()
	{
		left = null;
		right = null;
	}
	
	Cells( LispData left , LispData right )
	{
		if ( left == null )
		{
			throw new NullArgumentException( "left" );
		}
		if ( right == null )
		{
			throw new NullArgumentException( "right" );
		}
		this.left = left;
		this.right = right;
	}

	@Override
	boolean isAtom()
	{
		return ( isNil() ) ;
	}
	
	@Override
	boolean isNil()
	{
		return ( left == null && right == null) ;
	}
	
	@Override
	LispData car()
	{
		return ( left == null ? Cells.getNil() : left ) ;
	}
	
	@Override
	LispData cdr()
	{
		return ( right == null ? Cells.getNil() : right ) ;
	}
	
	@Override
	public String toString()
	{
		if ( isNil() )
		{
			return ( "()" ) ;
		}
		else
		{
			if ( right.isNil() )
			{
				return ( "(" + left.toString() + ")" ) ;
			}
			else if ( right.isAtom() )
			{
				return ( "(" + left.toString() + "." + right.toString() + ")" ) ;
			}
			else
			{
				return ( "(" + left.toString() + " " + right.toString() + ")" ) ;
			}
		}
	}
}