package myapp.lisp;

import myapp.lisp.CUI;
import myapp.lisp.Engine;
import myapp.lisp.Parser;

public class Lisp
{
	public static void main( String[] args ) throws java.lang.Exception
	{
		Lisp lisp = new Lisp();
		CUI cui = new CUI(lisp);
		
		cui.start();
	}
	
	private final int VERSION_MAJOR    = 0;
	private final int VERSION_MINOR    = 1;
	private final int VERSION_REVISION = 0;
	
	Lisp() {
	}
	
	Parser getParser()
	{
		return ( new Parser() ) ;
	}
	
	Engine getEngine()
	{
		return ( new Engine() ) ;
	}
	
	String getVersion()
	{
		return ( VERSION_MAJOR + "." + VERSION_MINOR + "." + VERSION_REVISION );
	}
	
}