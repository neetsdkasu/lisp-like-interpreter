package myapp.lisp;

import myapp.lisp.LispData;
import myapp.util.NullArgumentException;

class Atom extends LispData
{
	private final String word;
	
	Atom( String word )
	{
		if ( word == null )
		{
			throw new NullArgumentException( "word" );
		}
		this.word = word;
	}
	
	@Override
	boolean isAtom()
	{
		return ( true ) ;
	}
	
	@Override
	boolean isNil()
	{
		return ( false ) ;
	}
	
	@Override
	LispData car()
	{
		return ( null ) ;
	}
	
	@Override
	LispData cdr()
	{
		return ( null ) ;
	}
	
	@Override
	public String toString()
	{
		return ( word ) ;
	}
	
	@Override
	public int hashCode()
	{
		return ( word.hashCode() ) ;
	}
	
	@Override
	public boolean equals( Object obj )
	{
		return ( word.equals( obj ) ) ;
	}
}