#
# Makefile
#

JC=javac
JR=jar
MK=make

SRCDIR=src
CLSDIR=classes
PKG=myapp\lisp
SRC=$(SRCDIR)\$(PKG)\Lisp.java
CLS=$(CLSDIR)\$(PKG)\Lisp.class
DEP=$(SRCDIR)\$(PKG)\*.java

AllFiles: $(CLS)

$(CLS): $(DEP)
	@if not exist $(CLSDIR) mkdir $(CLSDIR)
	$(JC) -sourcepath $(SRCDIR) -d $(CLSDIR) $(SRC)
